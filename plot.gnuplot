set terminal pdf enh size 29.7,21 font "Arial,6"
if (!exists("infile")) infile = 'scheduling_plans'
if (!exists("outfile")) outfile = infile . '.pdf'
set output outfile
unset key
plot \
 infile using ($4)     : ($2      ) : (0)         : (0)   : ($3) : yticlabel (1) with vectors nohead lc variable lw 10, \
 infile using ($4)     : ($2 - 0.4) : (1 + $5-$4) : (0)   : ($3) with vectors nohead lc variable lw 2,\
 infile using ($4)     : ($2 + 0.4) : (1 + $5-$4) : (0)   : ($3) with vectors nohead lc variable lw 2,\
 infile using ($4)     : ($2 - 0.4) : (0)         : (0.8) : ($3) with vectors nohead lc variable lw 2,\
 infile using (1 + $5) : ($2 - 0.4) : (0)         : (0.8) : ($3) with vectors nohead lc variable lw 2;
